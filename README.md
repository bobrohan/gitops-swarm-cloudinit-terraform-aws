# GitOps Swarm
## cloudinit, terraform, aws

Single node master+worker swarm for implementation of GitOps

Simple crontab running `git pull; docker stack deploy`, see init.cfg.

## Prereqs 

Add the following to `terraform.tfvars` 

```
accesskey=
secretkey=
dreamteam-pubkey=
dreamteam-dburi=
```

## One command deploy
 
`terraform apply --auto-approve`